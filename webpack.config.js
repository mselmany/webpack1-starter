"use strict";

var webpack = require("webpack"),
    path = require("path"),
    HtmlWebpackPlugin = require("html-webpack-plugin"),
    HtmlWebpackHarddiskPlugin = require("html-webpack-harddisk-plugin"),
    ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    context: path.resolve(__dirname, "public"),
    entry: ["./app/index.ts",
        "./assets/styles/index.css",
        "./index.pug"],
    output: {
        path: path.resolve(__dirname, "public/dist"),
        filename: "bundle.js",
        publicPath: "/assets/"
    },
    resolve: {
        extensions: ["", ".js", ".ts", ".tsx", "css"]
    },
    module: {
        loaders: [
            {
                test: /\.ts(x?)$/,
                loader: "ts-loader"
            },
            {
                test: /\.pug$/,
                loader: "pug-loader"
            },
            {
                test: /\.(png|gif|jpe?g|svg)$/i,
                loader: "url-loader?limit=10000"
            },
            {
                test: /.*\.(png|gif|jpe?g|svg)$/i,
                loaders: [
                    "file-loader?hash=sha512&digest=hex&name=[hash].[ext]",
                    "image-webpack-loader?{optimizationLevel: 7, interlaced: false, pngquant:{quality: '65-90', speed: 4}, mozjpeg: {quality: 65}}"
                ]
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    "style-loader",
                    "css-loader?importLoaders=1!postcss-loader")
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackHarddiskPlugin(),
        new ExtractTextPlugin("style.css"),
        new HtmlWebpackPlugin({
            template: "index.pug",
            // cache: false,
            alwaysWriteToDisk: true
        })
    ],
    watch: true,
    devtool: "source-map",
    devServer: {
        hot: true,
        inline: true,
        contentBase: path.resolve(__dirname, "public/dist")
    }
};